# ADAndor conda recipe

Home: "https://github.com/areaDetector/ADAndor"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS ADAndor module
